Name:           jaxb-fi
Version:        2.1.0
Release:        2%{?dist}
Summary:        Implementation of the Fast Infoset Standard for Binary XML
# jaxb-fi is licensed ASL 2.0 and EDL-1.0 (BSD)
# bundled org.apache.xerces.util.XMLChar.java is licensed ASL 1.1
License:        ASL 2.0 and BSD and ASL 1.1
URL:            https://github.com/eclipse-ee4j/jaxb-fi
BuildArch:      noarch

Source0:        %{url}/archive/%{version}/%{name}-%{version}.tar.gz

BuildRequires:  maven-local
BuildRequires:  mvn(junit:junit)
BuildRequires:  mvn(org.apache.felix:maven-bundle-plugin)
BuildRequires:  mvn(org.apache.maven.plugins:maven-assembly-plugin)
BuildRequires:  mvn(org.apache.maven.plugins:maven-dependency-plugin)
BuildRequires:  mvn(org.codehaus.mojo:build-helper-maven-plugin)

%description
Fast Infoset Project, an Open Source implementation of the Fast Infoset
Standard for Binary XML.

The Fast Infoset specification (ITU-T Rec. X.891 | ISO/IEC 24824-1)
describes an open, standards-based "binary XML" format that is based on
the XML Information Set.

%package tests
License:        ASL 2.0 and BSD
Summary:        FastInfoset Roundtrip Tests
%description tests
%{summary}.

%prep
%setup -q

find -name 'module-info.java' -type f -delete

%pom_remove_parent

%pom_disable_module samples
%pom_disable_module utilities

%pom_remove_plugin :buildnumber-maven-plugin
%pom_remove_plugin :glassfish-copyright-maven-plugin
%pom_remove_plugin :maven-enforcer-plugin

%mvn_package :FastInfosetRoundTripTests tests

%build
# Javadoc fails: error: too many module declarations found
%mvn_build -j

%install
%mvn_install

%files -f .mfiles
%license LICENSE NOTICE.md
%doc README.md

%files tests -f .mfiles-tests
%license LICENSE NOTICE.md

%changelog
* Wed Feb 01 2023 Marián Konček <mkoncek@redhat.com> - 2.1.0-2
- Reduce dependencies, reorganize subpackages

* Tue Jan 17 2023 Marian Koncek <mkoncek@redhat.com> - 2.1.0-1
- Initial build
